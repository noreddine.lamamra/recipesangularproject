import {Component, OnDestroy, OnInit} from '@angular/core';
import {map, Subscription} from "rxjs";
import * as fromApp from '../store/app.reducer';
import {Store} from "@ngrx/store";
import * as AuthActions from "../auth/store/auth.actions";
import * as RecipesActions from '../recipes/store/recipe.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  isAuth: boolean = false;

  constructor(private store: Store<fromApp.AppState>) {
  }

  onSaveData() {
    this.store.dispatch(new RecipesActions.StoreRecipes());
  }

  onFetchData() {
    this.store.dispatch(new RecipesActions.FetchRecipes());
  }

  ngOnInit(): void {
    this.sub = this.store.select('auth').pipe(map(authState => authState.user)).subscribe(user => {
      this.isAuth = !!user;
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  onLogout() {
    this.store.dispatch(new AuthActions.Logout());
  }
}
