import {Actions, createEffect, ofType} from "@ngrx/effects";
import * as AuthActions from './auth.actions';
import {catchError, map, of, switchMap, tap} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../user.model";
import {AuthService} from "../auth.service";

export interface AuthResponseData {
  king: string,
  idToken: string,
  email: string,
  refreshToken: string,
  expiresIn: string,
  localId: string,
  registered?: boolean
}

@Injectable()
export class AuthEffects {
  private signUpUrl: string = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + environment.fireBaseAPIKey;
  private singInUrl: string = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + environment.fireBaseAPIKey;

  authLogin = createEffect(() => {
    return this.actions$.pipe(ofType(AuthActions.LOGIN_START), switchMap((authData: AuthActions.LoginStart) => {
      return this.sendRequest(authData, this.singInUrl);
    }));
  })
  authRedirect = createEffect(() => {
    return this.actions$.pipe(ofType(AuthActions.AUTHENTICATE_SUCCESS), tap((authSuccessAction: AuthActions.AuthenticateSuccess) => {
      if (authSuccessAction.payload.redirect)
        this.router.navigate(['/']);
    }))
  }, {dispatch: false});

  authSignup = createEffect(() => {
    return this.actions$.pipe(ofType(AuthActions.SIGNUP_START), switchMap((authData: AuthActions.SignupStart) => {
      return this.sendRequest(authData, this.signUpUrl);
    }));
  });

  autoLogin = createEffect(() => {
    return this.actions$.pipe(ofType(AuthActions.AUTO_LOGIN), map(() => {
      const userData: { email: string, id: string, _token: string, _tokenExpirationDate: string } = JSON.parse(localStorage.getItem('userData'));
      if (!userData) {
        return {type: 'DUMMY'};
      }
      const loadedUser: User = new User(userData.email, userData.id, userData._token, new Date(userData._tokenExpirationDate));
      if (loadedUser.token) {
        const expirationDuration = new Date(userData._tokenExpirationDate).getTime() - new Date().getTime();
        this.authService.setLogoutTimer(expirationDuration);
        return new AuthActions.AuthenticateSuccess({
          email: loadedUser.email,
          userId: loadedUser.id,
          token: loadedUser.token,
          expirationDate: new Date(userData._tokenExpirationDate),
          redirect: false
        });
      }
      return {type: 'DUMMY'}
    }))
  });

  authLogout = createEffect(() => {
    return this.actions$.pipe(ofType(AuthActions.LOGOUT), tap(() => {
      this.authService.clearLogoutTimer();
      localStorage.removeItem('userData');
      this.router.navigate(['/auth']);
    }));
  }, {dispatch: false})

  sendRequest = (authData, url) => {
    return this.http.post<AuthResponseData>(url, {
      email: authData.payload.email,
      password: authData.payload.password,
      returnSecureToken: true
    }).pipe(tap(resData => {
      this.authService.setLogoutTimer(+resData.expiresIn * 1000);
    }), map(resData => {
      return this.handleAuthentication(resData.email, resData.localId, resData.idToken, +resData.expiresIn);
    }), catchError(errorRes => {
      return this.handleError(errorRes);
    }),);
  }

  handleAuthentication = (email: string, userId: string, token: string, expiresIn: number) => {
    const date = new Date(new Date().getTime() + expiresIn * 1000);
    const user: User = new User(email, userId, token, date);
    localStorage.setItem('userData', JSON.stringify(user));
    return new AuthActions.AuthenticateSuccess({
      email,
      userId,
      token,
      expirationDate: date,
      redirect: true
    });
  }
  handleError = (errorRes) => {
    let errorMessage = "An unknown error occurred!";
    if (!errorRes.error || !errorRes.error.error)
      return of(new AuthActions.AuthenticateFail(errorMessage));
    switch (errorRes.error.error.message) {
      case 'EMAIL_EXISTS':
        errorMessage = 'This email already exists';
        break;
      case 'EMAIL_NOT_FOUND' :
        errorMessage = 'This email does not exist';
        break;
      case 'INVALID_PASSWORD' :
        errorMessage = 'This password is not correct'
        break;
    }
    return of(new AuthActions.AuthenticateFail(errorMessage));
  }

  constructor(private actions$: Actions, private http: HttpClient, private router: Router, private authService: AuthService) {
  }
}
