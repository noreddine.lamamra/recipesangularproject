import {Actions, createEffect, ofType} from "@ngrx/effects";
import * as RecipeActions from './recipe.actions';
import {map, switchMap, withLatestFrom} from "rxjs";
import {Recipe} from "../recipe.model";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromApp from '../../store/app.reducer';

@Injectable()
export class RecipeEffects {
  private url: string = "https://recipebookapp-58d96-default-rtdb.firebaseio.com/";

  constructor(private actions$: Actions, private http: HttpClient, private store: Store<fromApp.AppState>) {
  }

  fetchRecipes = createEffect(() => {
    return this.actions$.pipe(ofType(RecipeActions.FETCH_RECIPES), switchMap(() => {
      return this.http.get<Recipe[]>(this.url + "recipes.json")
    }), map(recipes => {
      return recipes.map(recipe => {
        return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
      });
    }), map(recipes => {
      return new RecipeActions.SetRecipes(recipes);
    }));
  });

  storeRecipes = createEffect(() => {
    return this.actions$.pipe(ofType(RecipeActions.STORE_RECIPES), withLatestFrom(this.store.select('recipes')),
      switchMap(([actionData, recipesState]) => {
        return this.http.put(this.url + "recipes.json", recipesState.recipes);
      }));
  }, {dispatch: false})
}
